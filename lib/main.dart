import 'package:flutter/material.dart';
import 'package:users/page/page_1.dart';
import 'package:users/storage/album_storage.dart';
import 'package:users/storage/comment_storage.dart';
import 'package:users/storage/post_storage.dart';
import 'package:users/storage/user_storage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AlbumStorage.instance.initilize();
  await CommentStorage.instance.initilize();
  await PostStorage.instance.initilize();
  await UserStorage.instance.initilize();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => PageOne(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green,
      body: Center(
        child: Container(
          child: Text(
            'Users',
            style: TextStyle(
                fontSize: 100,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.bold,
                color: Colors.yellow),
          ),
          transform: Matrix4.rotationZ(-0.3),
        ),
      ),
    );
  }
}
