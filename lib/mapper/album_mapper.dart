import 'package:users/model/album.dart';

extension AlbumMapper on Album {
  static const userIDKey = 'userID';
  static const idKey = 'id';
  static const titleKey = 'title';

  static Album fromMap(Map map) {
    return Album(
      userID: map[userIDKey] as int,
      id: map[idKey] as int,
      title: map[titleKey] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      userIDKey: userID,
      idKey: id,
      titleKey: title,
    };
  }
}

extension AlbumsMapper on List<Album> {
  static List<Album> fromMaps(List maps) {
    return maps.map<Album>((e) => AlbumMapper.fromMap(e)).toList();
  }

  List toMaps() {
    return this.map((e) => e.toMap()).toList();
  }
}
