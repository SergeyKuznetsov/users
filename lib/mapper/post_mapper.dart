import 'package:users/model/post.dart';

extension PostMapper on Post {
  static const userIDKey = 'userID';
  static const idKey = 'id';
  static const titleKey = 'title';
  static const bodyKey = 'body';

  static Post fromMap(Map map) {
    return Post(
      userID: map[userIDKey] as int,
      id: map[idKey] as int,
      title: map[titleKey] as String,
      body: map[bodyKey] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      userIDKey: userID,
      idKey: id,
      titleKey: title,
      bodyKey: body,
    };
  }
}

extension PostsMapper on List<Post> {
  static List<Post> fromMaps(List maps) {
    return maps.map<Post>((e) => PostMapper.fromMap(e)).toList();
  }

  List toMaps() {
    return this.map((e) => e.toMap()).toList();
  }
}
