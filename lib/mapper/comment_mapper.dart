import 'package:users/model/comment.dart';

extension CommentMapper on Comment {
  static const postIdKey = 'postId';
  static const idKey = 'id';
  static const nameKey = 'name';
  static const emailKey = 'email';
  static const bodyKey = 'body';

  static Comment fromMap(Map map) {
    return Comment(
      postId: map[postIdKey] as int,
      id: map[idKey] as int,
      name: map[nameKey] as String,
      email: map[emailKey] as String,
      body: map[bodyKey] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      postIdKey: postId,
      idKey: id,
      nameKey: name,
      emailKey: email,
      bodyKey: body,
    };
  }
}

extension CommentsMapper on List<Comment> {
  static List<Comment> fromMaps(List maps) {
    return maps.map<Comment>((e) => CommentMapper.fromMap(e)).toList();
  }

  List toMaps() {
    return this.map((e) => e.toMap()).toList();
  }
}
