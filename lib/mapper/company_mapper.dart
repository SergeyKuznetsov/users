import 'package:users/model/company.dart';

extension CompanyMapper on Company {
  static const nameKey = 'name';
  static const catchPhraseKey = 'catchPhrase';
  static const bsKey = 'bs';

  static Company fromMap(Map map) {
    return Company(
      name: map[nameKey] as String,
      catchPhrase: map[catchPhraseKey] as String,
      bs: map[bsKey] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      nameKey: name,
      catchPhraseKey: catchPhrase,
      bsKey: bs,
    };
  }
}
