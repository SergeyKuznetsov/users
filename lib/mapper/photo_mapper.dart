import 'package:users/model/photo.dart';

extension PhotoMapper on Photo {
  static const albumIdKey = 'albumId';
  static const idKey = 'id';
  static const titleKey = 'title';
  static const urlKey = 'url';
  static const thumbnailUrlKey = 'thumbnailUrl';

  static Photo fromMap(Map map) {
    return Photo(
      albumId: map[albumIdKey] as int,
      id: map[idKey] as int,
      title: map[titleKey] as String,
      url: map[urlKey] as String,
      thumbnailUrl: map[thumbnailUrlKey] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      albumIdKey: albumId,
      idKey: id,
      titleKey: title,
      urlKey: url,
      thumbnailUrlKey: thumbnailUrl,
    };
  }
}

extension PhotosMapper on List<Photo> {
  static List<Photo> fromMaps(List maps) {
    return maps.map<Photo>((e) => PhotoMapper.fromMap(e)).toList();
  }

  List toMaps() {
    return this.map((e) => e.toMap()).toList();
  }
}
