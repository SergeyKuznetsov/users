import 'package:users/model/geo.dart';

extension GeoMapper on Geo {
  static const latKey = 'lat';
  static const lngKey = 'lng';

  static Geo fromMap(Map map) {
    return Geo(
      lat: map[latKey] as String,
      lng: map[lngKey] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      latKey: lat,
      lngKey: lng,
    };
  }
}
