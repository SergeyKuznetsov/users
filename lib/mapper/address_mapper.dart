import 'package:users/mapper/geo_mapper.dart';
import 'package:users/model/address.dart';

extension AddressMapper on Address {
  static const streetKey = 'street';
  static const suiteKey = 'suite';
  static const cityKey = 'city';
  static const zipcodeKey = 'zipcode';
  static const geoKey = 'geo';

  static Address fromMap(Map map) {
    return Address(
      street: map[streetKey] as String,
      suite: map[suiteKey] as String,
      city: map[cityKey] as String,
      zipcode: map[zipcodeKey] as String,
      geo: GeoMapper.fromMap(map[geoKey] as Map),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      streetKey: street,
      suiteKey: suite,
      cityKey: city,
      zipcodeKey: zipcode,
      geoKey: geo.toMap(),
    };
  }
}
