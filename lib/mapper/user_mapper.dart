import 'package:users/mapper/address_mapper.dart';
import 'package:users/mapper/company_mapper.dart';
import 'package:users/model/user.dart';

extension UserMapper on User {
  static const idKey = 'id';
  static const nameKey = 'name';
  static const usernameKey = 'username';
  static const emailKey = 'email';
  static const addressKey = 'address';
  static const phoneKey = 'phone';
  static const websiteKey = 'website';
  static const companyKey = 'company';

  static User fromMap(Map map) {
    return User(
      id: map[idKey] as int,
      name: map[nameKey] as String,
      username: map[usernameKey] as String,
      email: map[emailKey] as String,
      address: AddressMapper.fromMap(map[addressKey] as Map),
      phone: map[phoneKey] as String,
      website: map[websiteKey] as String,
      company: CompanyMapper.fromMap(map[companyKey] as Map),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      idKey: id,
      nameKey: name,
      usernameKey: username,
      emailKey: email,
      addressKey: address.toMap(),
      phoneKey: phone,
      websiteKey: website,
      companyKey: company.toMap(),
    };
  }
}

extension UsersMapper on List<User> {
  static List<User> fromMaps(List maps) {
    return maps.map<User>((e) => UserMapper.fromMap(e)).toList();
  }

  List<Map> toMaps() {
    return this.map((e) => e.toMap()).toList();
  }
}
