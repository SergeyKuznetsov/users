import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:users/mapper/album_mapper.dart';
import 'package:users/model/album.dart';

class AlbumStorage {
  AlbumStorage._();

  static final instance = AlbumStorage._();

  SharedPreferences _preferences;

  static const _key = 'albums';

  Future<void> initilize() async {
    _preferences = await SharedPreferences.getInstance();
  }

  Future<void> saveAlbums(List<Album> albums) async {
    final savedAlbums = getAlbums();
    savedAlbums.addAll(albums);
    await _preferences.setStringList(
      _key,
      savedAlbums.map((e) => jsonEncode(e.toMap())).toList(),
    );
  }

  List<Album> getAlbums() {
    final source = _preferences.getStringList(_key) ?? [];
    return source
        .map((e) => AlbumMapper.fromMap(jsonDecode(e) as Map))
        .toList();
  }
}
