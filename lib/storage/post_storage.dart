import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:users/mapper/post_mapper.dart';
import 'package:users/model/post.dart';

class PostStorage {
  PostStorage._();

  static final instance = PostStorage._();

  SharedPreferences _preferences;

  static const _key = 'posts';

  Future<void> initilize() async {
    _preferences = await SharedPreferences.getInstance();
  }

  Future<void> savePosts(List<Post> posts) async {
    final savedPosts = getPosts();
    savedPosts.addAll(posts);
    await _preferences.setStringList(
      _key,
      savedPosts.map((e) => jsonEncode(e.toMap())).toList(),
    );
  }

  List<Post> getPosts() {
    final source = _preferences.getStringList(_key) ?? [];
    return source.map((e) => PostMapper.fromMap(jsonDecode(e) as Map)).toList();
  }
}
