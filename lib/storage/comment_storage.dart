import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:users/mapper/comment_mapper.dart';
import 'package:users/model/comment.dart';

class CommentStorage {
  CommentStorage._();

  static final instance = CommentStorage._();

  SharedPreferences _preferences;

  static const _key = 'comments';

  Future<void> initilize() async {
    _preferences = await SharedPreferences.getInstance();
  }

  Future<void> saveComment(Comment comment) async {
    final comments = getComments();
    comments.add(comment);
    await _preferences.setStringList(
      _key,
      comments.map((e) => jsonEncode(e.toMap())).toList(),
    );
  }

  Future<void> saveComments(List<Comment> comments) async {
    final savedComments = getComments();
    savedComments.addAll(comments);
    await _preferences.setStringList(
      _key,
      savedComments.map((e) => jsonEncode(e.toMap())).toList(),
    );
  }

  List<Comment> getComments() {
    final source = _preferences.getStringList(_key) ?? [];
    return source
        .map((e) => CommentMapper.fromMap(jsonDecode(e) as Map))
        .toList();
  }
}
