import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:users/mapper/user_mapper.dart';
import 'package:users/model/user.dart';

class UserStorage {
  UserStorage._();

  static final instance = UserStorage._();

  SharedPreferences _preferences;

  static const _key = 'users';

  Future<void> initilize() async {
    _preferences = await SharedPreferences.getInstance();
  }

  Future<void> saveUsers(List<User> users) async {
    final savedUsers = getUsers();
    savedUsers.addAll(users);
    await _preferences.setStringList(
      _key,
      savedUsers.map((e) => jsonEncode(e.toMap())).toList(),
    );
  }

  List<User> getUsers() {
    final source = _preferences.getStringList(_key) ?? [];
    return source.map((e) => UserMapper.fromMap(jsonDecode(e) as Map)).toList();
  }
}
