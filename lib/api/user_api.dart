import 'dart:convert';

import 'package:users/api/api.dart';
import 'package:users/model/user.dart';
import 'package:users/mapper/user_mapper.dart';
import 'package:http/http.dart' as http;

class UserApi {
  static const _endPoint = 'users';

  Future<List<User>> getUsers() async {
    http.Response response = await http.get(Uri.parse(Api.url + _endPoint));

    if (response.statusCode == 200) {
      return UsersMapper.fromMaps(json.decode(response.body));
    } else {
      throw Exception('Status code: ${response.statusCode}');
    }
  }
}
