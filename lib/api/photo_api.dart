import 'dart:convert';

import 'package:users/api/api.dart';
import 'package:users/mapper/photo_mapper.dart';
import 'package:http/http.dart' as http;
import 'package:users/model/photo.dart';

class PhotoApi {
  static const _endPoint = 'photos';

  Future<List<Photo>> getPhotos(int albumId) async {
    final response = await http.get(
      Uri.parse(Api.url + _endPoint + '?albumId=$albumId'),
    );
    if (response.statusCode == 200) {
      return PhotosMapper.fromMaps(json.decode(response.body));
    } else {
      throw Exception('Status code: ${response.statusCode}');
    }
  }
}
