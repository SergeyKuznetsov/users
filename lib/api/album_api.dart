import 'dart:convert';

import 'package:users/api/api.dart';
import 'package:users/model/album.dart';
import 'package:users/mapper/album_mapper.dart';
import 'package:http/http.dart' as http;

class AlbumApi {
  static const _endPoint = 'albums';

  Future<List<Album>> getAlbums(int userId) async {
    http.Response response =
        await http.get(Uri.parse(Api.url + _endPoint + '?userId=$userId'));

    if (response.statusCode == 200) {
      return AlbumsMapper.fromMaps(json.decode(response.body));
    } else {
      throw Exception('Status code: ${response.statusCode}');
    }
  }
}
