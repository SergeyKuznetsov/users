import 'dart:convert';

import 'package:users/api/api.dart';
import 'package:users/model/post.dart';
import 'package:users/mapper/post_mapper.dart';
import 'package:http/http.dart' as http;

class PostApi {
  static const _endPoint = 'posts';

  Future<List<Post>> getPosts(int userId) async {
    http.Response response =
        await http.get(Uri.parse(Api.url + _endPoint + '?userId=$userId'));

    if (response.statusCode == 200) {
      return PostsMapper.fromMaps(json.decode(response.body));
    } else {
      throw Exception('Status code: ${response.statusCode}');
    }
  }
}
