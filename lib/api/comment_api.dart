import 'dart:convert';

import 'package:users/api/api.dart';
import 'package:users/mapper/comment_mapper.dart';
import 'package:users/model/comment.dart';
import 'package:http/http.dart' as http;

class CommentApi {
  static const _endPoint = 'comments';

  Future<List<Comment>> getComments(int postId) async {
    final response = await http.get(
      Uri.parse(Api.url + _endPoint + '?postId=$postId'),
    );
    if (response.statusCode == 200) {
      return CommentsMapper.fromMaps(json.decode(response.body));
    } else {
      throw Exception('Status code: ${response.statusCode}');
    }
  }

  Future<void> sendComment(Comment comment) async {
    final response = await http.post(
      Uri.parse(Api.url + _endPoint),
      body: comment.toMap().toString(),
    );
    if (response.statusCode == 201) {
      print(response.body);
      return;
    } else {
      throw Exception('Status code: ${response.statusCode}');
    }
  }
}
