class Geo {
  const Geo({this.lat, this.lng});

  final String lat;
  final String lng;
}
