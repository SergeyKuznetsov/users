class Album {
  const Album({
    this.userID,
    this.id,
    this.title,
  });

  final int userID;
  final int id;
  final String title;
}
