class Company {
  const Company({
    this.name,
    this.catchPhrase,
    this.bs,
  });

  final String name;
  final String catchPhrase;
  final String bs;
}
