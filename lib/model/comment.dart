class Comment {
  const Comment({
    this.postId,
    this.id,
    this.name,
    this.email,
    this.body,
  });

  final int postId;
  final int id;
  final String name;
  final String email;
  final String body;
}
