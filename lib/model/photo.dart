class Photo {
  const Photo({this.albumId, this.id, this.title, this.url, this.thumbnailUrl});

  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;
}
