class Post {
  const Post({
    this.userID,
    this.id,
    this.title,
    this.body,
  });

  final int userID;
  final int id;
  final String title;
  final String body;
}
