import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:users/api/comment_api.dart';
import 'package:users/model/comment.dart';
import 'package:users/model/post.dart';
import 'package:users/page/comment_page.dart';
import 'package:users/storage/comment_storage.dart';

class PageFive extends StatefulWidget {
  PageFive({this.post});

  final Post post;

  @override
  State<StatefulWidget> createState() => _PageFiveState();
}

class _PageFiveState extends State<PageFive> {
  final commentApi = CommentApi();

  Future<List<Comment>> _getComments() async {
    final comments = CommentStorage.instance
        .getComments()
        .where((e) => e.postId == widget.post.id)
        .toList();
    if (comments.isNotEmpty) return comments;
    final fetchedComments = await commentApi.getComments(widget.post.id);
    await CommentStorage.instance.saveComments(fetchedComments);
    return fetchedComments;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[200],
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: Text(
          'Комментарии',
          style: TextStyle(fontSize: 30, color: Colors.green),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView(
          children: [
            Column(
              children: [
                SizedBox(height: 20),
                Text(
                  'Пост',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.green[700],
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    widget.post.title,
                    style: TextStyle(fontWeight: FontWeight.w900),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child: Text(widget.post.body)),
                SizedBox(height: 30),
                Text(
                  'Комментарии',
                  style: TextStyle(fontSize: 20, color: Colors.green[700]),
                ),
              ],
            ),
            FutureBuilder<List<Comment>>(
                future: _getComments(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Center(child: errorWidget());
                  }
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        ...snapshot.data
                            .sublist(0)
                            .map(
                              (e) => ListTile(
                                title: Text(e.name + '\n' + e.email),
                                subtitle: Text(
                                  e.body,
                                  maxLines: 1,
                                ),
                                onTap: () {},
                              ),
                            )
                            .toList(),
                      ],
                    );
                  }
                  return Center(child: loadingWidget());
                }),
            ElevatedButton(
              onPressed: () async {
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return CommentPage(
                        postId: widget.post.id,
                      );
                    },
                  ),
                );
                setState(() {});
              },
              child: Text(
                'Комментировать',
                style: TextStyle(fontSize: 20, color: Colors.yellow),
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.green),
              ),
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  Widget errorWidget() {
    return Text('ошибка');
  }

  Widget loadingWidget() {
    return CircularProgressIndicator();
  }
}
