import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:users/api/post_api.dart';
import 'package:users/model/album.dart';
import 'package:users/model/post.dart';
import 'package:users/model/user.dart';
import 'package:users/api/album_api.dart';
import 'package:users/page/page_3.dart';
import 'package:users/page/page_4.dart';
import 'package:users/storage/album_storage.dart';
import 'package:users/storage/post_storage.dart';

class PageTwo extends StatefulWidget {
  PageTwo({@required this.user});

  final User user;

  @override
  _PageTwoState createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  final postApi = PostApi();
  final albumApi = AlbumApi();

  Future<List<Post>> _getPosts() async {
    final posts = PostStorage.instance
        .getPosts()
        .where((e) => e.userID == widget.user.id)
        .toList();
    if (posts.isNotEmpty) return posts;
    final fetchedPosts = await postApi.getPosts(widget.user.id);
    await PostStorage.instance.savePosts(fetchedPosts);
    return fetchedPosts;
  }

  Future<List<Album>> _getAlbums() async {
    final albums = AlbumStorage.instance
        .getAlbums()
        .where((e) => e.userID == widget.user.id)
        .toList();
    if (albums.isNotEmpty) return albums;
    final fetchedAlbums = await albumApi.getAlbums(widget.user.id);
    await AlbumStorage.instance.saveAlbums(fetchedAlbums);
    return fetchedAlbums;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[200],
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: Text(
          widget.user.username,
          style: TextStyle(fontSize: 30, color: Colors.green),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView(children: [
          Column(
            children: [
              SizedBox(height: 10),
              Text(
                widget.user.name,
                style: TextStyle(fontSize: 20),
              ),
              Text(widget.user.email),
              Text(widget.user.phone),
              Text(widget.user.website),
              SizedBox(height: 10),
              Text(
                'Working(company):',
                style: TextStyle(decoration: TextDecoration.underline),
              ),
              Text(widget.user.company.name),
              Text(widget.user.company.bs),
              Text(
                widget.user.company.catchPhrase,
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
              SizedBox(height: 10),
              Text(widget.user.address.street),
              Text(widget.user.address.suite),
              Text(widget.user.address.city),
              Text(widget.user.address.zipcode),
              Text(widget.user.address.geo.lat),
              Text(widget.user.address.geo.lng),
              SizedBox(
                height: 20,
              ),
              Text(
                'Посты',
                style: TextStyle(
                  fontSize: 17,
                  color: Colors.green[700],
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
          FutureBuilder<List<Post>>(
              future: _getPosts(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Center(child: errorWidget());
                }
                if (snapshot.hasData) {
                  final posts = snapshot.data;
                  return Column(
                    children: [
                      ...posts
                          .sublist(0, min(3, posts.length))
                          .map(
                            (e) => ListTile(
                              title: Text(e.title),
                              subtitle: Text(
                                e.body,
                                maxLines: 1,
                              ),
                            ),
                          )
                          .toList(),
                      TextButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return PageThree(posts: snapshot.data);
                          }));
                        },
                        child: Text(
                          'Посмотреть все посты',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.green[700],
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.italic,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                    ],
                  );
                }
                return Center(child: loadingWidget());
              }),
          SizedBox(height: 20),
          Align(
            alignment: Alignment.center,
            child: Text(
              'Альбомы',
              style: TextStyle(
                fontSize: 17,
                color: Colors.green[700],
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          FutureBuilder<List<Album>>(
              future: _getAlbums(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Center(child: errorWidget());
                }
                if (snapshot.hasData) {
                  final albums = snapshot.data;
                  return Column(
                    children: [
                      ...albums
                          .sublist(0, min(3, albums.length))
                          .map(
                            (e) => ListTile(
                              title: Text(e.title),
                            ),
                          )
                          .toList(),
                      TextButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return PageFour(albums: snapshot.data);
                          }));
                        },
                        child: Text(
                          'Посмотреть все альбомы',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.green[700],
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.italic,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                      SizedBox(height: 40),
                    ],
                  );
                }
                return Center(child: loadingWidget());
              }),
        ]),
      ),
    );
  }

  Widget errorWidget() {
    return Text('ошибка');
  }

  Widget loadingWidget() {
    return CircularProgressIndicator();
  }
}
