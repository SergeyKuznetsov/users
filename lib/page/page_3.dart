import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:users/model/post.dart';
import 'package:users/page/page_5.dart';

class PageThree extends StatelessWidget {
  const PageThree({this.posts});

  final List<Post> posts;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: Text(
          'Посты',
          style: TextStyle(fontSize: 30, color: Colors.green),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView.builder(
            itemCount: posts.length,
            itemBuilder: (context, index) {
              final post = posts[index];
              return Card(
                child: ListTile(
                  title: Text(post.title),
                  subtitle: Text(
                    post.body,
                    maxLines: 1,
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return PageFive(post: post);
                    }));
                  },
                ),
                color: Colors.yellow[200],
              );
            }),
      ),
    );
  }
}
