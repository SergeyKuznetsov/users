import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:users/model/album.dart';
import 'package:users/page/page_6.dart';

class PageFour extends StatelessWidget {
  const PageFour({this.albums});

  final List<Album> albums;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: Text(
          'Альбомы',
          style: TextStyle(fontSize: 30, color: Colors.green),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView.builder(
            itemCount: albums.length,
            itemBuilder: (context, index) {
              final album = albums[index];
              return Card(
                child: ListTile(
                  title: Text(album.title),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return PageSix(
                        albumId: album.id,
                        album: albums[index],
                      );
                    }));
                  },
                ),
                color: Colors.yellow[200],
              );
            }),
      ),
    );
  }
}
