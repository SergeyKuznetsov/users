import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:users/api/photo_api.dart';
import 'package:users/model/photo.dart';

class PageSix extends StatelessWidget {
  final album;
  final photoApi = PhotoApi();
  final int albumId;

  PageSix({this.albumId, this.album});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: Text(
          'фото',
          style: TextStyle(fontSize: 30, color: Colors.green),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView(children: [
          SizedBox(height: 15),
          Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            child: Text(
              album.title,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 15),
          FutureBuilder<List<Photo>>(
              future: photoApi.getPhotos(albumId),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Center(child: errorWidget());
                }
                if (snapshot.hasData) {
                  return Column(
                    children: [
                      ...snapshot.data
                          .sublist(0)
                          .map(
                            (e) => Card(
                              child: ListTile(
                                title: Text(e.title),
                                subtitle: Image.network(e.url),
                                leading: Image.network(e.thumbnailUrl),
                              ),
                              color: Colors.yellow[200],
                            ),
                          )
                          .toList(),
                    ],
                  );
                }
                return Center(child: loadingWidget());
              }),
        ]),
      ),
    );
  }

  Widget errorWidget() {
    return Text('ошибка');
  }

  Widget loadingWidget() {
    return CircularProgressIndicator();
  }
}
