import 'package:flutter/material.dart';
import 'package:users/api/user_api.dart';
import 'package:users/model/user.dart';
import 'package:users/page/page_2.dart';
import 'package:users/storage/user_storage.dart';

class PageOne extends StatelessWidget {
  final userApi = UserApi();

  Future<List<User>> _getUsers() async {
    final users = UserStorage.instance.getUsers();
    if (users.isNotEmpty) return users;
    final fetchedUsers = await userApi.getUsers();
    await UserStorage.instance.saveUsers(fetchedUsers);
    return fetchedUsers;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: Text(
          'Users',
          style: TextStyle(fontSize: 30, color: Colors.green),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: FutureBuilder<List<User>>(
            future: _getUsers(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text('${snapshot.error}'),
                );
              }
              if (snapshot.hasData) {
                final loadedUsers = snapshot.data;
                return ListView.builder(
                    itemCount: loadedUsers.length,
                    itemBuilder: (context, index) {
                      return Card(
                        child: ListTile(
                          title: Text(loadedUsers[index].username),
                          subtitle: Text(loadedUsers[index].name),
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return PageTwo(user: loadedUsers[index]);
                            }));
                          },
                        ),
                        color: Colors.yellow[200],
                      );
                    });
              }
              return Center(
                child: loadingWidget(),
              );
            }),
      ),
    );
  }

  Widget loadingWidget() {
    return CircularProgressIndicator();
  }
}
