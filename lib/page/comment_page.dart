import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:users/api/comment_api.dart';
import 'package:users/model/comment.dart';
import 'package:users/storage/comment_storage.dart';

// ignore: must_be_immutable
class CommentPage extends StatelessWidget {
  CommentPage({this.postId});

  String _name = '';
  String _email = '';
  String _body = '';

  final int postId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: Text(
          'коммент',
          style: TextStyle(fontSize: 30, color: Colors.green),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView(
          children: [
            SizedBox(height: 5.0),
            Container(
              margin: EdgeInsets.all(5.0),
              child: TextField(
                onChanged: (name) => _name = name,
                decoration: InputDecoration(
                  fillColor: Colors.yellow[200],
                  filled: true,
                  hintText: 'Имя',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                ),
                keyboardType: TextInputType.name,
              ),
            ),
            Container(
              margin: EdgeInsets.all(5.0),
              child: TextField(
                onChanged: (email) => _email = email,
                decoration: InputDecoration(
                  fillColor: Colors.yellow[200],
                  filled: true,
                  hintText: 'Почта',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                ),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            Container(
              margin: EdgeInsets.all(5.0),
              child: TextField(
                onChanged: (body) => _body = body,
                decoration: InputDecoration(
                  fillColor: Colors.yellow[200],
                  filled: true,
                  hintText: 'Комментарий',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                ),
                maxLines: 9,
                keyboardType: TextInputType.text,
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                _name = _name.trim();
                _email = _email.trim();
                _body = _body.trim();

                if (_name == '' || _email == '' || _body == '') {
                  return;
                }

                final commentApi = CommentApi();
                final id = Random().nextInt(10000);
                final comment = Comment(
                  postId: postId,
                  id: id,
                  name: _name,
                  email: _email,
                  body: _body,
                );
                await commentApi.sendComment(comment);
                await CommentStorage.instance.saveComment(comment);
                Navigator.pop(context);
              },
              child: Text(
                'Отправить',
                style: TextStyle(fontSize: 20, color: Colors.yellow),
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.green),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
